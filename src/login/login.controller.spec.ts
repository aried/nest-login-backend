import { Test, TestingModule } from '@nestjs/testing';
import { LoginController } from './login.controller';
import { LoginService } from './login.service';

describe('LoginController', () => {
  let controller: LoginController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [LoginController],
      providers: [LoginService],
    }).compile();

    controller = module.get<LoginController>(LoginController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  it('should be return alright', () => {
    let credentials = { username: 'aried', password: 'wakawaku' };
    let result = controller.login(credentials);
    expect(result.valid).toBe(true);
  });
  it('should be return not alright', () => {
    let credentials = { username: 'aried', password: 'wakuwaku' };
    let result = controller.login(credentials);
    expect(result.valid).toBe(false);
  });
});
