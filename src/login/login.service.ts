import { Inject, Injectable } from '@nestjs/common';

@Injectable()
export class LoginService {
  constructor() {}

  login(credentials: { username: string; password: string }) {
    if (
      credentials.username === 'aried' &&
      credentials.password === 'wakawaku'
    ) {
      return {
        valid: true,
        message: 'login success',
        token: 'thisIsToken',
      };
    }
    return {
      valid: false,
      message: 'login failed',
      token: '',
    };
  }
}
