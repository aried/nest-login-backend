import { Body, Controller, Post } from '@nestjs/common';
import { LoginService } from './login.service';
import { MessagePattern } from '@nestjs/microservices';

@Controller('login')
export class LoginController {
  constructor(private readonly loginService: LoginService) {}

  @MessagePattern({ internalCmd: 'login' })
  login(@Body() credentials: { username: string; password: string }) {
    return this.loginService.login(credentials);
  }
}
