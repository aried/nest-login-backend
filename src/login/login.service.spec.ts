import { Test, TestingModule } from '@nestjs/testing';
import { LoginService } from './login.service';
import { LoginModule } from './login.module';

describe('LoginService', () => {
  let service: LoginService;
  let login: (credentials: { username: string; password: string }) => {
    valid: boolean;
    message: string;
    token: string;
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LoginService],
    }).compile();

    service = module.get<LoginService>(LoginService);
    login = service.login;
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  it('should be ok', () => {
    expect(login({ username: 'aried', password: 'wakawaku' }).valid).toBe(true);
  });
  it('should be ko', () => {
    expect(login({ username: 'aried', password: 'wakuwaku' }).valid).toBe(
      false,
    );
  });
});
