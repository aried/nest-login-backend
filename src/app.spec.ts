import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoginModule } from './login/login.module';
import { LoginController } from './login/login.controller';
import { LoginService } from './login/login.service';
import { AppModule } from './app.module';

describe('AppModule', () => {
  it('should test app module', async () => {
    const module = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    expect(module).toBeDefined();
    expect(module.get(AppService)).toBeInstanceOf(AppService);
    expect(module.get(AppController)).toBeInstanceOf(AppController);
  });
});

describe('AppTester', () => {
  let appModule: AppModule;
  let appService: AppService;
  let appController: AppController;
  let loginModule: LoginModule;
  let loginService: LoginService;
  let loginController: LoginController;

  beforeEach(async () => {
    appModule = await Test.createTestingModule({
      imports: [LoginModule],
      controllers: [AppController],
      providers: [AppService],
    }).compile();
  });

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [LoginModule],
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appService = app.get<AppService>(AppService);
    appController = app.get<AppController>(AppController);
    loginModule = app.get<LoginModule>(LoginModule);

    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [LoginController],
      providers: [LoginService],
    }).compile();
    loginService = module.get<LoginService>(LoginService);
    loginController = module.get<LoginController>(LoginController);
  });

  describe('root', () => {
    describe('root defined', () => {
      it('should defined module', () => {
        expect(appModule).toBeDefined();
      });
      it('should defined service', () => {
        expect(appService).toBeDefined();
      });
      it('should defined controller', () => {
        expect(appController).toBeDefined();
      });
    });
    describe('root do sumthing', () => {
      it('should return "Hello World!"', () => {
        expect(appController.getHello()).toBe('Hello World!');
      });
    });
  });
  describe('login', () => {
    describe('login module', () => {
      it('should defined module', () => {
        expect(loginModule).toBeDefined();
      });
    });
    describe('login service', () => {
      it('should defined the service function', () => {
        expect(loginService).toBeDefined();
        expect(loginService.login).toBeDefined();
      });
      it('should return sumthing from service', () => {
        let correct = {
          username: 'aried',
          password: 'wakawaku',
        };
        let incorrect = {
          username: 'aried',
          password: 'wakuwaku',
        };
        expect(loginService.login(correct).valid).toBe(true);
        expect(loginService.login(incorrect).valid).toBe(false);
      });
    });
    describe('login controller', () => {
      it('should defined the controller function', () => {
        expect(loginController).toBeDefined();
        expect(loginController.login).toBeDefined();
      });
      it('should return sumthing from controller', () => {
        let correct = {
          username: 'aried',
          password: 'wakawaku',
        };
        let incorrect = {
          username: 'aried',
          password: 'wakuwaku',
        };
        expect(loginController.login(correct).valid).toBe(true);
        expect(loginController.login(incorrect).valid).toBe(false);
      });
    });
  });
});
